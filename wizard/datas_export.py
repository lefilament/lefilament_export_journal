# -*- coding: utf-8 -*-

# Copyright 2004-2015 Odoo S.A.
# (exporting code inspired from web/controllers originally licensed under LGPL)
# Copyright 2019 Le Filament (<http://www.le-filament.com>)
# License GPL-3 or later (http://www.gnu.org/licenses/gpl.html).

from datetime import datetime
import csv
import re
from cStringIO import StringIO

from odoo import models, fields, api, http
from odoo.addons.web.controllers.main import serialize_exception
from odoo.addons.web.controllers.main import content_disposition
from odoo.http import request
from odoo.tools.misc import xlwt

HEADER_DEFAULT = [
    'date',
    'journal',
    'compte',
    'debit',
    'credit',
    'libelle',
    'piece',
    'echeance',
    'ref_piece'
    ]

HEADER_DEFAULT_COALA = [
    'date',
    'journal',
    'compte',
    'ref_piece',
    'libelle',
    'debit',
    'credit',
    'monnaie',
    'section_anal',
    ]

HEADER_DEFAULT_COALA_FILE = [
    'Date',
    'Journal',
    'Compte',
    'Pièce',
    'Libellé',
    'Débit',
    'Crédit',
    'Monnaie',
    'Section analytique (champ 20)',
    ]


class AccountDatasExportWizard(models.TransientModel):
    _name = "invoice.line.export"

    def _get_journal(self):
        lst = [('ventes', 'Ventes'),('achats', 'Achats')]
        module_hr = self.env['ir.module.module'].sudo().search([
            ('name', '=', 'hr_expense')
        ])
        module_pos = self.env['ir.module.module'].sudo().search([
            ('name', '=', 'point_of_sale')
        ])
        if module_hr and module_hr.state == 'installed':
            lst.append(('frais', 'Notes de Frais'))
        if module_pos and module_pos.state == 'installed':
            lst.append(('caisses', 'Caisses'))
        return lst

    date_start = fields.Date('Date de début', required=True)
    date_end = fields.Date('Date de fin',
                           required=True,
                           default=datetime.today().strftime(
                               '%Y-%m-%d'))
    export_format = fields.Selection([('csv', 'CSV'), ('xls', 'Excel')],
                                     'Format', default="csv")
    journal = fields.Selection(_get_journal, 'Journal', default='ventes')

    @api.multi
    def get_data_file(self):
        nom_outil_compta = self.env['ir.values'].get_default('account.config.settings', 'nom_outil_compta')
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/export_journal?format=%s&journal=%s&nom_outil_compta=%s' %
                   (self.export_format, self.journal, nom_outil_compta) +
                   '&date_start=%s&date_end=%s' %
                   (self.date_start, self.date_end),
            'target': 'new',
            }


class AccountDatasExport(http.Controller):
    def export_csv(self, lignes_export, header, filename_, nom_outil_compta):
        fp = StringIO()

        export_file = csv.writer(fp, delimiter=';', quoting=csv.QUOTE_ALL)
        # export_file.writerow(header)

        row = []
        # Add header line only if outil = coala
        if nom_outil_compta == 'coala':
            header_file = HEADER_DEFAULT_COALA_FILE
            for head in header_file:
                row.append(head)
            export_file.writerow(row)

        for line in lignes_export:
            row = []
            for h in header:
                if isinstance(line[h], unicode):
                    try:
                        value = line[h].encode('utf-8')
                    except UnicodeError:
                        pass
                else:
                    if (h == 'date') or (h == 'echeance'):
                        if line[h]:
                            value = datetime.strptime(line[h], '%Y-%m-%d')\
                                .strftime('%d/%m/%Y')
                        else:
                            value = line[h]
                    elif (h == 'credit') or (h == 'debit'):
                        value = str(line[h]).replace('.', ',')
                    else:
                        value = line[h]
                row.append(value)

            export_file.writerow(row)

        fp.seek(0)
        data = fp.read()
        fp.close()

        filename = filename_ + '.csv'

        csvhttpheaders = [
            ('Content-Type', 'text/csv;charset=utf8'),
            ('Content-Disposition', content_disposition(filename)),
        ]

        return request.make_response(data, headers=csvhttpheaders)

    def export_xls(self, lignes_export, header, filename_, nom_outil_compta):
        workbook = xlwt.Workbook()
        worksheet = workbook.add_sheet(filename_)

        if nom_outil_compta == 'coala':
            header_file = HEADER_DEFAULT_COALA_FILE
        else:
            header_file = header

        for i, fieldname in enumerate(header_file):
            worksheet.write(0, i, fieldname)
            worksheet.col(i).width = 8000  # around 220 pixels

        base_style = xlwt.easyxf('align: wrap yes')

        for row_index, line in enumerate(lignes_export):
            for cell_index, h in enumerate(header):
                cell_style = base_style
                if (h == 'date') or (h == 'echeance'):
                    if line[h]:
                        cell_value = datetime.strptime(line[h], '%Y-%m-%d')\
                            .strftime('%d/%m/%Y')
                    else:
                        cell_value = line[h]
                elif (h == 'credit') or (h == 'debit'):
                    cell_value = line[h]
                elif isinstance(line[h], basestring):
                    cell_value = re.sub("\r", " ", line[h])
                else:
                    cell_value = line[h]
                worksheet.write(row_index + 1, cell_index,
                                cell_value, cell_style)

        fp = StringIO()
        workbook.save(fp)
        fp.seek(0)
        data = fp.read()
        fp.close()

        filename = filename_ + '.xls'

        csvhttpheaders = [
            ('Content-Type', 'text/csv;charset=utf8'),
            ('Content-Disposition', content_disposition(filename)),
        ]

        return request.make_response(data, headers=csvhttpheaders)

    def datas_export_ventes(self, format, date_start, date_end, nom_outil_compta):
        if nom_outil_compta == 'ibiza':
            header = HEADER_DEFAULT
        elif nom_outil_compta == 'coala':
            header = HEADER_DEFAULT_COALA
        else:
            header = HEADER_DEFAULT

        # requete
        if nom_outil_compta == 'ibiza':
          request.cr.execute("""
              SELECT l.date,
               'VT' AS journal,
               a.code AS compte,
               'E' AS monnaie,
               l.debit,
               l.credit,
               c.name AS section_anal,
               (CASE WHEN l.name = '/'
                THEN CONCAT(p.name,' - ','Facture ',i.number)
                ELSE CONCAT(p.name,' - ','Facture ',i.number,' - ',l.name) END) AS libelle,
               i.number AS piece,
               l.date_maturity AS echeance,
               i.number AS ref_piece
              FROM account_move_line AS l
              LEFT JOIN account_invoice AS i ON l.invoice_id = i.id
              LEFT JOIN account_account AS a ON l.account_id = a.id
              LEFT JOIN account_analytic_line AS b ON l.id = b.move_id
              LEFT JOIN account_analytic_account AS c ON c.id = b.account_id
              LEFT JOIN res_partner AS p ON l.partner_id = p.id
              WHERE l.journal_id = 1 AND l.date >= %s AND l.date <= %s
              ORDER BY l.date, l.move_id, a.code DESC;
              """, (date_start, date_end))
        else:
          request.cr.execute("""
              SELECT l.date,
               'VT' AS journal,
               a.code AS compte,
               'E' AS monnaie,
               l.debit,
               l.credit,
               c.name AS section_anal,
               (CASE WHEN l.name = '/'
                THEN a.name
                ELSE CONCAT('Facture ',i.number,' - ',l.name) END) AS libelle,
               i.number AS piece,
               l.date_maturity AS echeance,
               i.number AS ref_piece
              FROM account_move_line AS l
              LEFT JOIN account_invoice AS i ON l.invoice_id = i.id
              LEFT JOIN account_account AS a ON l.account_id = a.id
              LEFT JOIN account_analytic_line AS b ON l.id = b.move_id
              LEFT JOIN account_analytic_account AS c ON c.id = b.account_id
              WHERE l.journal_id = 1 AND l.date >= %s AND l.date <= %s
              ORDER BY l.date, l.move_id, a.code DESC;
              """, (date_start, date_end))
        lignes_export = request.cr.dictfetchall()

        company_name = request.env['res.company'].search([('id', '=', 1)]).name
        filename_ = (company_name.title().replace(' ', '')
                     + 'JournalVentes_' + date_start.replace('-', '')
                     + '_' + date_end.replace('-', ''))

        if format == 'csv':
            return self.export_csv(lignes_export, header, filename_, nom_outil_compta)

        return self.export_xls(lignes_export, header, filename_, nom_outil_compta)

    def datas_export_achats(self, format, date_start, date_end, nom_outil_compta):
        if nom_outil_compta == 'ibiza':
            header = HEADER_DEFAULT
            journal_value = 'HA'
        elif nom_outil_compta == 'coala':
            header = HEADER_DEFAULT_COALA
            journal_value = 'AC'
        else:
            header = HEADER_DEFAULT
            journal_value = 'HA'

        request.cr.execute("""
            SELECT l.date,
             %s AS journal,
             a.code AS compte,
             'E' AS monnaie,
             l.debit,
             l.credit,
             c.name AS section_anal,
             (CASE WHEN l.name = '/'
              THEN a.name
              ELSE CONCAT('Facture ',i.number,' - ',l.name) END) AS libelle,
             i.number AS piece,
             i.date_due AS echeance,
             i.reference AS ref_piece
            FROM account_move_line AS l
            LEFT JOIN account_invoice AS i ON l.invoice_id = i.id
            LEFT JOIN account_account AS a ON l.account_id = a.id
            LEFT JOIN account_analytic_line AS b ON l.id = b.move_id
            LEFT JOIN account_analytic_account AS c ON c.id = b.account_id
            WHERE l.journal_id = 2 AND i.type='in_invoice'
             AND l.date >= %s AND l.date <= %s
            ORDER BY l.date, l.move_id, a.code DESC;
            """, (journal_value, date_start, date_end))
        lignes_export = request.cr.dictfetchall()

        company_name = request.env['res.company'].search([('id', '=', 1)]).name
        filename_ = (company_name.title().replace(' ', '')
                     + 'JournalAchats_' + date_start.replace('-', '')
                     + '_' + date_end.replace('-', ''))

        if format == 'csv':
            return self.export_csv(lignes_export, header, filename_, nom_outil_compta)

        return self.export_xls(lignes_export, header, filename_, nom_outil_compta)

    def datas_export_frais(self, format, date_start, date_end, nom_outil_compta):
        if nom_outil_compta == 'ibiza':
            header = HEADER_DEFAULT
        elif nom_outil_compta == 'coala':
            header = HEADER_DEFAULT_COALA
        else:
            header = HEADER_DEFAULT

        request.cr.execute("""
            SELECT l.date,
             'NDF' AS journal,
             a.code AS compte,
             'E' AS monnaie,
             l.debit,
             l.credit,
             c.name AS section_anal,
             (CASE WHEN l.user_type_id = 2
              THEN a.name
              ELSE CONCAT(m.ref,' - ', l.name) END) AS libelle,
             m.name AS piece,
             '' AS echeance,
             m.narration AS ref_piece
            FROM account_move_line AS l
            LEFT JOIN account_account AS a ON l.account_id = a.id
            LEFT JOIN account_journal AS j ON l.journal_id = j.id
            LEFT JOIN account_move AS m ON m.id = l.move_id
            LEFT JOIN account_analytic_line AS b ON l.id = b.move_id
            LEFT JOIN account_analytic_account AS c ON c.id = b.account_id
            WHERE j.code = 'NDF' AND l.date >= %s AND l.date <= %s
            ORDER BY l.date, l.move_id, a.code DESC;
            """, (date_start, date_end))
        lignes_export = request.cr.dictfetchall()

        company_name = request.env['res.company'].search([('id', '=', 1)]).name
        filename_ = (company_name.title().replace(' ', '')
                     + 'NotesDeFrais_' + date_start.replace('-', '')
                     + '_' + date_end.replace('-', ''))

        if format == 'csv':
            return self.export_csv(lignes_export, header, filename_, nom_outil_compta)

        return self.export_xls(lignes_export, header, filename_, nom_outil_compta)

    def datas_export_caisses(self, format, date_start, date_end, nom_outil_compta):
        if nom_outil_compta == 'ibiza':
            header = HEADER_DEFAULT
        elif nom_outil_compta == 'coala':
            header = HEADER_DEFAULT_COALA
        else:
            header = HEADER_DEFAULT

        request.cr.execute("""
            SELECT l.date,
             'PDV' AS journal,
             a.code AS compte,
             'E' AS monnaie,
             l.debit,
             l.credit,
             c.name AS section_anal,
             (CASE WHEN l.user_type_id = 1
              THEN a.name
              ELSE CONCAT('Vente ',l.ref,' - ',l.name) END) AS libelle,
             m.name AS piece,
             '' AS echeance,
             l.ref AS ref_piece
            FROM account_move_line AS l
            LEFT JOIN account_account AS a ON l.account_id = a.id
            LEFT JOIN account_journal AS j ON l.journal_id = j.id
            LEFT JOIN account_move AS m ON m.id = l.move_id
            LEFT JOIN account_analytic_line AS b ON l.id = b.move_id
            LEFT JOIN account_analytic_account AS c ON c.id = b.account_id
            WHERE j.code = 'POSS' AND l.date >= %s AND l.date <= %s
            ORDER BY l.date, l.move_id, a.code DESC;
            """, (date_start, date_end))
        lignes_export = request.cr.dictfetchall()

        company_name = request.env['res.company'].search([('id', '=', 1)]).name
        filename_ = (company_name.title().replace(' ', '')
                     + 'Caisses_' + date_start.replace('-', '')
                     + '_' + date_end.replace('-', ''))

        if format == 'csv':
            return self.export_csv(lignes_export, header, filename_, nom_outil_compta)

        return self.export_xls(lignes_export, header, filename_, nom_outil_compta)

    @http.route('/web/export_journal/', type='http', auth="user")
    @serialize_exception
    def datas_export(self, format, journal, nom_outil_compta, date_start, date_end, **kw):
        if journal == 'ventes':
            return self.datas_export_ventes(format, date_start, date_end, nom_outil_compta)
        elif journal == 'achats':
            return self.datas_export_achats(format, date_start, date_end, nom_outil_compta)
        elif journal == 'frais':
            return self.datas_export_frais(format, date_start, date_end, nom_outil_compta)
        elif journal == 'caisses':
            return self.datas_export_caisses(format, date_start, date_end, nom_outil_compta)
