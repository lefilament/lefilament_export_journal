# Copyright 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class FilResConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    nom_outil_compta = fields.Selection([('ibiza', 'Ibiza'),
                                         ('coala', 'Coala')],
                                        'Outil Comptable')

    @api.multi
    def set_nom_outil_compta(self):
        return self.env['ir.values'].sudo().set_default(
            'account.config.settings', 'nom_outil_compta', self.nom_outil_compta)