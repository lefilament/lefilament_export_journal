# -*- coding: utf-8 -*-

{
    'name': 'Export Journal Ibiza',
    'summary': 'Export des journaux pour import dans Ibiza',
    'description': 'Export des journaux pour import dans Ibiza',
    'author': 'LE FILAMENT',
    'version': '10.0.1.0.0',
    'license': "GPL-3",
    'depends': ['account', 'hr_expense'],
    'qweb': [],
    'data': [
        'views/res_config_settings.xml',
        'wizard/datas_export_view.xml',
    ],
}
