.. image:: https://img.shields.io/badge/licence-GPL--3-blue.svg
   :target: http://www.gnu.org/licenses/gpl
   :alt: License: GPL-3


====================
Export Journal Ibiza
====================

Export des journaux pour import dans Ibiza (outil utilis� par notre comtpable)



Credits
=======

Contributors
------------

* Benjamin Rivier <benjamin@le-filament.com>
* Odoo S.A. (modified hr_expense function and export functions inspiration)

Maintainer
----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
